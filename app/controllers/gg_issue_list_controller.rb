class GgIssueListController < ApplicationController
  before_filter :find_project_by_project_id
  def index
    if params[:tracker]
      @tracker = Tracker.find(params[:tracker])
    else
      @tracker = Tracker.all.first
    end
    @issue = Issue.new
    @issue.project = @project
    @issue.tracker = @tracker
    @custom_fields = @issue.visible_custom_field_values
    @trackermenu = @project.module_enabled?(:gg_trackermenu)
    @status = IssueStatus.all
  end

  def main
    if params[:tracker]
      @tracker = Tracker.find(params[:tracker])
    else
      @tracker = Tracker.all.first
    end
    @issue = Issue.new
    @issue.project = @project
    @issue.tracker = @tracker
    @custom_fields = @issue.visible_custom_field_values
    @trackermenu = @project.module_enabled?(:gg_trackermenu)
    @status = IssueStatus.all
  end

  def eti_shouwen
    @tracker = Tracker.find_by("name = '收文处理'")
    @f_tracker = Tracker.find_by("name = '发文处理'")
    @issue = Issue.new
    @issue.project = @project
    @issue.tracker = @tracker
    @custom_fields = @issue.visible_custom_field_values
    @trackermenu = @project.module_enabled?(:gg_trackermenu)
    @status = IssueStatus.all
  end

  def eti_fawen
    @tracker = Tracker.find_by("name = '发文处理'")
    @s_tracker = Tracker.find_by("name = '收文处理'")
    @issue = Issue.new
    @issue.project = @project
    @issue.tracker = @tracker
    @custom_fields = @issue.visible_custom_field_values
    @trackermenu = @project.module_enabled?(:gg_trackermenu)
    @status = IssueStatus.all
  end

  def get_eti_shouwen

  end

  def get_eti_fawen

  end

  def issues_by_subject
    @subject = params[:subject]
  end

end