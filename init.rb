Redmine::Plugin.register :gg_issue_list do
  name 'Issuelist plugin'
  author 'Author name'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  # project_module :gg_issue_list do
  #   permission :gg_issue_list_view,
  #              {:gg_issue_list => [ :index]
  #              },
  #              :require => :member
  # end

  # menu :project_menu, :gg_issue_list, {:controller => 'gg_issue_list', :action => 'index' }, :caption => :label_issue_list ,
  #      :before => :setting, :param =>:project_id

end
