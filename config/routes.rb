# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

RedmineApp::Application.routes.draw do
  match "/projects/:project_id/issues/list" => 'gg_issue_list#index', :as => :gg_issue_list_index, via:[:get]
  match "/projects/:project_id/issues/main" => 'gg_issue_list#main', :as => :gg_issue_list_main, via:[:get]
  match "/projects/:project_id/issues/eti_fawen" => 'gg_issue_list#eti_fawen', :as => :gg_issue_list_eti_fawen, via:[:get]
  match "/projects/:project_id/issues/eti_shouwen" => 'gg_issue_list#eti_shouwen', :as => :gg_issue_list_eti_shouwen, via:[:get]
  match "/projects/:project_id/issues/subject" => 'gg_issue_list#issues_by_subject', :as => :gg_issue_list_subject, via:[:get]
end

